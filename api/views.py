from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Posyandu, Balita, Ibu, Bidan, HasilKontrol, Imunisasi, IbuHamil
from .serializers import PosyanduSerializer, BalitaSerializer, IbuSerializer, BidanSerializer, HasilKontrolSerializer, ImunisasiSerializer, IbuHamilSerializer
from rest_framework.views import APIView

class PosyanduListCreate(generics.ListCreateAPIView):
    queryset = Posyandu.objects.all()
    serializer_class = PosyanduSerializer

    def delete(self, request, *args, **kwargs):
        Posyandu.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class PosyanduRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Posyandu.objects.all()
    serializer_class = PosyanduSerializer
    lookup_field = "pk"

class BalitaListCreate(generics.ListCreateAPIView):
    queryset = Balita.objects.all()
    serializer_class = BalitaSerializer

    def delete(self, request, *args, **kwargs):
        Balita.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class BalitaRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Balita.objects.all()
    serializer_class = BalitaSerializer
    lookup_field = "pk"

class IbuListCreate(generics.ListCreateAPIView):
    queryset = Ibu.objects.all()
    serializer_class = IbuSerializer

    def delete(self, request, *args, **kwargs):
        Ibu.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class IbuRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ibu.objects.all()
    serializer_class = IbuSerializer
    lookup_field = "pk"

class BidanListCreate(generics.ListCreateAPIView):
    queryset = Bidan.objects.all()
    serializer_class = BidanSerializer

    def delete(self, request, *args, **kwargs):
        Bidan.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class BidanRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bidan.objects.all()
    serializer_class = BidanSerializer
    lookup_field = "pk"

class HasilKontrolListCreate(generics.ListCreateAPIView):
    queryset = HasilKontrol.objects.all()
    serializer_class = HasilKontrolSerializer

    def delete(self, request, *args, **kwargs):
        HasilKontrol.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class HasilKontrolRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = HasilKontrol.objects.all()
    serializer_class = HasilKontrolSerializer
    lookup_field = "pk"

class ImunisasiListCreate(generics.ListCreateAPIView):
    queryset = Imunisasi.objects.all()
    serializer_class = ImunisasiSerializer

    def delete(self, request, *args, **kwargs):
        Imunisasi.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class ImunisasiRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = Imunisasi.objects.all()
    serializer_class = ImunisasiSerializer
    lookup_field = "pk"

class IbuHamilListCreate(generics.ListCreateAPIView):
    queryset = IbuHamil.objects.all()
    serializer_class = IbuHamilSerializer

    def delete(self, request, *args, **kwargs):
        IbuHamil.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
  

class IbuHamilRetrieveUpdateDestroy(generics.RetrieveUpdateDestroyAPIView):
    queryset = IbuHamil.objects.all()
    serializer_class = IbuHamilSerializer
    lookup_field = "pk"
