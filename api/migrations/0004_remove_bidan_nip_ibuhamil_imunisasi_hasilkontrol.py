# Generated by Django 5.0.6 on 2024-07-02 15:37

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_rename_tanggal_lahir_balita_balita_tanggal_lahir_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bidan',
            name='NIP',
        ),
        migrations.CreateModel(
            name='IbuHamil',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status_kehamilan', models.CharField(max_length=255)),
                ('tanggal_perkiraan_lahir', models.DateField()),
                ('riwayat_kehamilan', models.TextField()),
                ('tanggal_kontrol', models.DateField(blank=True, null=True)),
                ('berat_badan', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('tekanan_darah', models.CharField(blank=True, max_length=20)),
                ('detak_jantung_janin', models.IntegerField(blank=True, null=True)),
                ('catatan', models.TextField(blank=True)),
                ('ibu', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.ibu')),
            ],
        ),
        migrations.CreateModel(
            name='Imunisasi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_vaksin', models.CharField(max_length=255)),
                ('tanggal_pemberian', models.DateField()),
                ('dosis', models.IntegerField()),
                ('balita', models.ManyToManyField(to='api.balita', verbose_name='Balita')),
            ],
        ),
        migrations.CreateModel(
            name='HasilKontrol',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tanggal_kontrol', models.DateField()),
                ('tinggi_badan', models.DecimalField(decimal_places=2, max_digits=5)),
                ('berat_badan', models.DecimalField(decimal_places=2, max_digits=5)),
                ('lingkar_kepala', models.DecimalField(decimal_places=2, max_digits=5)),
                ('balita', models.ManyToManyField(to='api.balita', verbose_name='Balita')),
                ('bidan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.bidan')),
                ('imunisasi', models.ManyToManyField(to='api.imunisasi')),
            ],
        ),
    ]
