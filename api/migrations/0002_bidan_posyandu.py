# Generated by Django 5.0.6 on 2024-07-02 13:43

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bidan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100, verbose_name='Nama Bidan')),
                ('balita', models.ManyToManyField(to='api.balita', verbose_name='Balita')),
            ],
        ),
        migrations.CreateModel(
            name='Posyandu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100, verbose_name='Nama Posyandu')),
                ('alamat', models.CharField(max_length=100, verbose_name='Alamat')),
                ('jadwal', models.CharField(max_length=100, verbose_name='Jadwal')),
                ('bidan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Posyandu', to='api.bidan', verbose_name='Bidan')),
            ],
        ),
    ]
