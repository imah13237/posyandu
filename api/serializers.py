from rest_framework import serializers
from .models import Posyandu, Balita, Ibu, Bidan, HasilKontrol, Imunisasi, IbuHamil

class PosyanduSerializer(serializers.ModelSerializer):
    class Meta:
        model = Posyandu
        fields = ["id",   "nama_posyandu", "jadwal", "alamat",  "kecamatan", "kota" ]

class BalitaSerializer(serializers.ModelSerializer):
   # Bidan = serializers.FrongKeyField(many=True)
    class Meta:
        model = Balita
        fields = ["id", "nama_balita", "tanggal_lahir", "jenis_kelamin", "ibu", "posyandu"]
    
class IbuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ibu
        fields = ["id", "nama_ibu", "desa", "kecamatan", "kota", "no_hp"]

class BidanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bidan
        fields = ["id", "nama_bidan",  "alamat", "no_hp", "posyandu"]

class HasilKontrolSerializer(serializers.ModelSerializer):
    class Meta:
        model = HasilKontrol
        fields = ["id", "tanggal_kontrol", "tinggi_badan", "berat_badan", "lingkar_kepala", "imunisasi",
                    "balita", "bidan"]

class ImunisasiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Imunisasi
        fields = ["id", "nama_vaksin", "tanggal_pemberian", "dosis", "balita"]

class IbuHamilSerializer(serializers.ModelSerializer):
    class Meta:
        model = IbuHamil
        fields = ["id", "ibu", "status_kehamilan", "tanggal_perkiraan_lahir", "riwayat_kehamilan", 
                    "tanggal_kontrol", "berat_badan", "tekanan_darah", "detak_jantung_janin", "catatan"]
