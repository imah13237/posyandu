from django.db import models


class Posyandu(models.Model):
    nama_posyandu = models.CharField(max_length=255)
    jadwal = models.CharField(max_length=100, verbose_name="Jadwal")
    alamat = models.TextField()
    kecamatan = models.CharField(max_length=255)
    kota = models.CharField(max_length=255)

    def __str__(self):
        return self.nama_posyandu

class Balita(models.Model):
    nama_balita = models.CharField(max_length=255)
    tanggal_lahir = models.DateField(verbose_name="Tanggal Lahir Balita")
    jenis_kelamin = models.CharField(max_length=10)
    ibu = models.ForeignKey('Ibu', on_delete=models.CASCADE)
    posyandu = models.ForeignKey('Posyandu', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_balita

class Ibu(models.Model):
    nama_ibu = models.CharField(max_length=255)
    desa = models.CharField(max_length=255)
    kecamatan = models.CharField(max_length=255)
    kota = models.CharField(max_length=255)
    no_hp = models.CharField(max_length=20)

    def __str__(self):
        return self.nama_ibu



class Bidan(models.Model):
    nama_bidan = models.CharField(max_length=255)
    alamat = models.TextField()
    no_hp = models.CharField(max_length=20)
    posyandu = models.ForeignKey('Posyandu', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_bidan

class HasilKontrol(models.Model):
    tanggal_kontrol = models.DateField()
    tinggi_badan = models.DecimalField(max_digits=5, decimal_places=2)
    berat_badan = models.DecimalField(max_digits=5, decimal_places=2)
    lingkar_kepala = models.DecimalField(max_digits=5, decimal_places=2)
    imunisasi = models.ManyToManyField('Imunisasi')
    balita = models.ManyToManyField(Balita, verbose_name="Balita")
    bidan = models.ForeignKey('Bidan', on_delete=models.CASCADE)

    def __str__(self):
        return self.tanggal_kontrol

class Imunisasi(models.Model):
    nama_vaksin = models.CharField(max_length=255)
    tanggal_pemberian = models.DateField()
    dosis = models.IntegerField()
    balita = models.ManyToManyField(Balita, verbose_name="Balita")

    def __str__(self):
        return self.nama_vaksin

class IbuHamil(models.Model):
    ibu = models.ForeignKey('Ibu', on_delete=models.CASCADE)  # Menghubungkan dengan model Ibu
    status_kehamilan = models.CharField(max_length=255)
    tanggal_perkiraan_lahir = models.DateField()
    riwayat_kehamilan = models.TextField()
    tanggal_kontrol = models.DateField(blank=True, null=True)
    berat_badan = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    tekanan_darah = models.CharField(max_length=20, blank=True)
    detak_jantung_janin = models.IntegerField(blank=True, null=True)
    catatan = models.TextField(blank=True)

    def __str__(self):
        return self.ibu


